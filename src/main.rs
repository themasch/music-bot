use teloxide::prelude::*;
use tokio::macros::support::Future;
use tokio::sync::mpsc::Sender;

mod mpv;

const THE_CHAT_ID: i64 = -332801465;
const SOCKET_PATH: &str = "/tmp/mpv.sock";

async fn mpv_queue_url(mut mpv: Sender<mpv::Command>, url: String) {
    let result = mpv
        .send(mpv::Command::LoadFile(url, mpv::QueueMode::AppendPlay))
        .await;

    if let Err(err) = result {
        log::error!("ERROR sending url {}", err);
    }
}

fn handle_mpv_message(message: mpv::Message) -> Option<String> {
    match &message {
        mpv::Message::Event(mpv::Event::PropertyChange { name, data, .. }) => match name.as_str() {
            "media-title" => Some(format!("now playing '{}'", data)),
            _ => None,
        },
        _ => None,
    }
}

#[tokio::main]
async fn main() {
    if dotenv::from_filename(".env.local").is_err() {
        dotenv::dotenv().ok();
    }

    teloxide::enable_logging!();
    log::info!("Starting music_bot...");

    let bot = Bot::from_env();

    let client = mpv::MpvClient::create(SOCKET_PATH);

    let (mut mpv_read, mut mpv_control) = client.start_control().await;

    // we want to get notified if the media-title changes
    let send_result = mpv_control
        .send(mpv::Command::ObserveProperty("media-title".to_string()))
        .await;

    if let Err(err) = send_result {
        log::error!("ERROR mpv(observe-property): {}", err);
    }

    let bot_clone = bot.clone();
    tokio::spawn(async move {
        while let Some(msg) = mpv_read.recv().await {
            log::info!("sending {:?} to channel", msg);
            if let Some(msg) = handle_mpv_message(msg) {
                let msg_res = bot_clone.send_message(THE_CHAT_ID, msg).send().await;

                if let Err(err) = msg_res {
                    log::error!("ERROR send_message {}", err);
                }
            }
        }
    });

    Dispatcher::new(bot)
        .messages_handler(move |rx| create_message_handler(rx, mpv_control))
        .dispatch()
        .await;
}

fn create_message_handler(
    rx: UnboundedReceiver<UpdateWithCx<Message>>,
    mpv_control: Sender<mpv::Command>,
) -> impl Future<Output = ()> {
    rx.for_each_concurrent(None, move |message: UpdateWithCx<Message>| {
        let mpv_control = mpv_control.clone();
        async move {
            if let Some(msg) = message.update.text_owned() {
                log::info!(" -> received: {}", msg);
                if msg.starts_with("https://") || msg.starts_with("http://") {
                    mpv_queue_url(mpv_control, msg).await;
                }
            }
        }
    })
}
