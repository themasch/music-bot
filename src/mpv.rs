use serde::ser::SerializeMap;
use serde::{Deserialize, Serialize, Serializer};
use std::path::Path;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::UnixStream;
use tokio::sync::mpsc::{channel, Receiver, Sender};

#[derive(Debug)]
#[allow(dead_code)]
pub enum QueueMode {
    Replace,
    AppendPlay,
}

impl QueueMode {
    fn as_str(&self) -> &str {
        match self {
            QueueMode::AppendPlay => "append-play",
            QueueMode::Replace => "replace",
        }
    }
}

#[derive(Debug)]
#[allow(dead_code)]
pub enum Command {
    LoadFile(String, QueueMode),
    ObserveProperty(String),
    ObservePropertyString(String),
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(tag = "event")]
pub enum Event {
    Unpause,
    Pause,
    VideoReconfig,
    AudioReconfig,
    PlaybackRestart,
    Seek,
    TracksChanged,
    StartFile,
    EndFile,
    MetadataUpdate,
    PropertyChange {
        id: usize,
        data: String,
        name: String,
    },
}

#[derive(Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(untagged)]
pub enum Message {
    Event(Event),
    Response { error: String, request_id: usize },
}

impl Serialize for Command {
    fn serialize<S>(&self, serializer: S) -> Result<<S as Serializer>::Ok, <S as Serializer>::Error>
    where
        S: Serializer,
    {
        let mut state = serializer.serialize_map(Some(1))?;
        match self {
            Command::LoadFile(url, mode) => {
                state
                    .serialize_entry("command", &["loadfile", url, mode.as_str()])
                    .unwrap();
            }
            Command::ObserveProperty(property) => {
                state
                    .serialize_entry("command", &("observe_property", 1, property.as_str()))
                    .unwrap();
            }
            Command::ObservePropertyString(property) => {
                state
                    .serialize_entry(
                        "command",
                        &("observe_property_string", 1, property.as_str()),
                    )
                    .unwrap();
            }
        };
        state.end()
    }
}

#[cfg(test)]
mod mpv_command_serialize_test {
    use crate::mpv::{Command, QueueMode};

    #[test]
    fn test_serialize_loadfile_command() {
        let cmd = Command::LoadFile("some-file.url".to_string(), QueueMode::Replace);
        assert_eq!(
            r#"{"command":["loadfile","some-file.url","replace"]}"#,
            &serde_json::to_string(&cmd).unwrap()
        );
        let cmd = Command::LoadFile("some-file.url".to_string(), QueueMode::AppendPlay);
        assert_eq!(
            r#"{"command":["loadfile","some-file.url","append-play"]}"#,
            &serde_json::to_string(&cmd).unwrap()
        );
    }

    #[test]
    fn test_serialize_observe_property_command() {
        let cmd = Command::ObserveProperty("property-name".to_string());
        assert_eq!(
            r#"{"command":["observe_property",1,"property-name"]}"#,
            &serde_json::to_string(&cmd).unwrap()
        );
    }

    #[test]
    fn test_serialize_observe_property_string_command() {
        let cmd = Command::ObservePropertyString("property-name".to_string());
        assert_eq!(
            r#"{"command":["observe_property_string",1,"property-name"]}"#,
            &serde_json::to_string(&cmd).unwrap()
        );
    }
}

pub struct MpvClient<P: AsRef<Path>> {
    socket_path: P,
}

impl<P: AsRef<Path>> MpvClient<P> {
    pub fn create(path: P) -> MpvClient<P> {
        MpvClient { socket_path: path }
    }

    pub async fn start_control(&self) -> (Receiver<Message>, Sender<Command>) {
        let (tx, mut rx) = channel(100);
        let (mut rtx, rrx) = channel(100);

        let mpv_stream = UnixStream::connect(&self.socket_path)
            .await
            .expect("connection to mpv failed");

        log::info!("got connection to mpv");

        let (mut mpv_read, mut mpv_write) = tokio::io::split(mpv_stream);

        tokio::spawn(async move {
            loop {
                if let Some(msg) = rx.recv().await {
                    log::info!("sending {:?}", msg);
                    let json = serde_json::to_string(&msg).unwrap();

                    log::info!("sending {}", json);
                    mpv_write
                        .write_all(format!("{}\n", json).as_bytes())
                        .await
                        .unwrap();
                }
            }
        });

        tokio::spawn(async move {
            log::info!("got connection to mpv");

            loop {
                let mut buffer = [0u8; 1024];
                let read = mpv_read.read(&mut buffer).await.unwrap();
                if read > 0 {
                    for chunk in buffer[0..read].split(|&c| c == b'\n') {
                        let cmd = serde_json::from_slice(chunk);
                        let output = String::from_utf8(chunk.to_vec()).unwrap();
                        log::info!("read {:?}: {:?}", output, cmd);
                        if let Ok(msg) = cmd {
                            rtx.send(msg).await.unwrap();
                        }
                    }
                }
            }
        });

        (rrx, tx)
    }
}
